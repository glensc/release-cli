package flags

const (
	// ServerURL available in the CLI context
	ServerURL = "server-url"
	// JobToken available in the CLI context
	JobToken = "job-token"
	// ProjectID available in the CLI context
	ProjectID = "project-id"
	// Name available in the CLI context
	Name = "name"
	// Description available in the CLI context
	Description = "description"
	// TagName available in the CLI context
	TagName = "tag-name"
	// Ref available in the CLI context
	Ref = "ref"
)
