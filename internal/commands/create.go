package commands

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/release-cli/internal/flags"
	"gitlab.com/gitlab-org/release-cli/internal/gitlab"
)

var defaultClientTimeout = 10 * time.Second

// Create defines the create command to be used by the CLI
func Create() *cli.Command {
	httpClient := &http.Client{
		Timeout: defaultClientTimeout,
	}

	return &cli.Command{
		Name:  "create",
		Usage: "Create a Release using GitLab's Releases API https://docs.gitlab.com/ee/api/releases/#create-a-release",
		Action: func(ctx *cli.Context) error {
			return createRelease(ctx, httpClient)
		},
		Subcommands: nil,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     flags.Name,
				Usage:    "The release name",
				Required: true,
			},
			&cli.StringFlag{
				Name:     flags.Description,
				Usage:    "The description of the release, you can use Markdown",
				Required: true,
			},
			&cli.StringFlag{
				Name:     flags.TagName,
				Usage:    "The tag the release will be created from",
				Required: true,
				EnvVars:  []string{"CI_COMMIT_TAG"},
			},
			&cli.StringFlag{
				Name:     flags.Ref,
				Usage:    "If tag_name doesn’t exist, the release will be created from ref; it can be a commit SHA, another tag name, or a branch name",
				Required: false,
				EnvVars:  []string{"CI_COMMIT_SHA"},
			},
		},
	}
}

func createRelease(ctx *cli.Context, httpClient gitlab.HTTPClient) error {
	projectID := ctx.String(flags.ProjectID)
	name := ctx.String(flags.Name)
	description := ctx.String(flags.Description)
	serverURL := ctx.String(flags.ServerURL)
	jobToken := ctx.String(flags.JobToken)
	tagName := ctx.String(flags.TagName)
	ref := ctx.String(flags.Ref)

	l := log.WithFields(log.Fields{
		"command":         ctx.Command.Name,
		flags.ServerURL:   serverURL,
		flags.ProjectID:   projectID,
		flags.Name:        name,
		flags.Description: description,
		flags.TagName:     tagName,
		flags.Ref:         ref,
	})

	l.Info("Creating Release...")

	gitlabClient, err := gitlab.New(serverURL, jobToken, projectID, httpClient)
	if err != nil {
		return fmt.Errorf("failed to create GitLab client: %w", err)
	}

	release, err := gitlabClient.CreateRelease(ctx.Context, &gitlab.CreateReleaseRequest{
		ID:          projectID,
		Name:        name,
		Description: description,
		TagName:     tagName,
		Ref:         ref,
	})
	if err != nil {
		return fmt.Errorf("failed to create release: %w", err)
	}

	printReleaseOutput(ctx.App.Writer, release)

	l.Info("release created successfully!")

	return nil
}

func printReleaseOutput(w io.Writer, release *gitlab.CreateReleaseResponse) {
	fmt.Fprintf(w, `

Tag: %s
Name: %s
Description: %s
Created At: %s
Released At: %s

`,
		release.TagName,
		release.Name,
		release.Description,
		release.CreatedAt,
		release.ReleasedAt,
	)

	fmt.Fprintf(w, "See all available releases here: %s/-/releases\n", os.Getenv("CI_PROJECT_URL"))
}
