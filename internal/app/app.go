package app

import (
	"time"

	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/release-cli/internal/commands"
	"gitlab.com/gitlab-org/release-cli/internal/flags"
)

// New creates cli.App for the releaser CLI
func New(version string) *cli.App {
	cli.HelpFlag = &cli.BoolFlag{
		Name:    "help",
		Aliases: []string{"h"},
		Usage:   "Show help",
	}
	cli.VersionFlag = &cli.BoolFlag{
		Name:    "version",
		Aliases: []string{"v"},
		Usage:   "Print the version"}

	return &cli.App{
		Name:     "release-cli",
		Usage:    "A CLI tool that interacts with GitLab's Releases API",
		Version:  version,
		HelpName: "help",
		Description: `
CLI tool that interacts with GitLab's Releases API https://docs.gitlab.com/ee/api/releases/.

All configuration flags will default to GitLab's CI predefined environment variables (https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).
To override these values, use the [GLOBAL OPTIONS].

Get started with release-cli https://gitlab.com/gitlab-org/release-cli.`,
		Commands: []*cli.Command{
			commands.Create(),
		},
		CommandNotFound: nil,
		OnUsageError:    nil,
		Compiled:        time.Now(),
		Authors: []*cli.Author{
			{
				Name:  "GitLab Inc.",
				Email: "support@gitlab.com",
			},
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        flags.ServerURL,
				Usage:       "The base URL of the GitLab instance, including protocol and port, for example https://gitlab.example.com:8080",
				Required:    true,
				EnvVars:     []string{"CI_SERVER_URL"},
				Destination: nil,
			},
			&cli.StringFlag{
				Name:        flags.JobToken,
				Usage:       "Job token used for authenticating with the GitLab Releases API",
				Required:    true,
				EnvVars:     []string{"CI_JOB_TOKEN"},
				Destination: nil,
			},
			&cli.StringFlag{
				Name:        flags.ProjectID,
				Usage:       "The current project's unique ID; used by GitLab CI internally",
				Required:    true,
				EnvVars:     []string{"CI_PROJECT_ID"},
				Destination: nil,
			},
		},
	}
}
