package app

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	got := New("version")
	require.NotNil(t, got)
	require.Equal(t, "release-cli", got.Name)
	require.Len(t, got.Commands, 1)
	require.Len(t, got.Flags, 3)
}
