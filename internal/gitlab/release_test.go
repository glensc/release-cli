package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/release-cli/internal/gitlab/testdata"
)

func TestClient_CreateRelease(t *testing.T) {
	baseCRR := CreateReleaseRequest{
		ID:          "projectID",
		Name:        "name",
		TagName:     "tag name",
		Description: "description",
		Ref:         "ref",
	}

	tests := []struct {
		name            string
		res             *http.Response
		wantResponse    *CreateReleaseResponse
		wantErrResponse *ErrorResponse
	}{
		{
			name: "success",
			res: &http.Response{
				StatusCode: http.StatusOK,
				Body:       ioutil.NopCloser(strings.NewReader(testdata.CreateReleaseSuccessResponse)),
			},
			wantResponse: func() *CreateReleaseResponse {
				var res CreateReleaseResponse
				err := json.Unmarshal([]byte(testdata.CreateReleaseSuccessResponse), &res)
				require.NoError(t, err)
				return &res
			}(),
		},
		{
			name: "forbidden",
			res: &http.Response{
				StatusCode: http.StatusForbidden,
				Body:       ioutil.NopCloser(strings.NewReader(testdata.CreateReleaseForbiddenResponse)),
			},
			wantErrResponse: &ErrorResponse{statusCode: http.StatusForbidden, Message: "403 Forbidden"},
		},
		{
			name: "bad_request",
			res: &http.Response{
				StatusCode: http.StatusBadRequest,
				Body:       ioutil.NopCloser(strings.NewReader(testdata.CreateReleaseBadRequestResponse)),
			},
			wantErrResponse: &ErrorResponse{statusCode: http.StatusBadRequest, Message: "tag_name is missing, description is missing"},
		},
		{
			name: "conflict",
			res: &http.Response{
				StatusCode: http.StatusConflict,
				Body:       ioutil.NopCloser(strings.NewReader(testdata.CreateReleaseConflictResponse)),
			},
			wantErrResponse: &ErrorResponse{statusCode: http.StatusConflict, Message: "Release already exists"},
		},
		{
			name: "internal_server_error",
			res: &http.Response{
				StatusCode: http.StatusInternalServerError,
				Body:       ioutil.NopCloser(strings.NewReader(testdata.CreateReleaseInternalErrorResponse)),
			},
			wantErrResponse: &ErrorResponse{statusCode: http.StatusInternalServerError, Message: "500 Internal Server Error"},
		},
		{
			name: "unexpected_error",
			res: &http.Response{
				StatusCode: http.StatusInternalServerError,
				Body:       ioutil.NopCloser(strings.NewReader(testdata.CreateReleaseUnexpectedErrorResponse)),
			},
			wantErrResponse: &ErrorResponse{statusCode: http.StatusInternalServerError, Err: "Something went wrong"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mhc := &MockHTTPClient{}
			mhc.On("Do", mock.Anything).Return(tt.res, nil).Once()

			gc := &Client{
				baseURL:    "http://127.0.0.1",
				token:      "privateToken",
				projectID:  "projectID",
				httpClient: mhc,
			}
			got, err := gc.CreateRelease(context.Background(), &baseCRR)
			if tt.wantErrResponse != nil {
				require.Error(t, err)
				require.EqualError(t, err, tt.wantErrResponse.Error())

				return
			}

			require.NoError(t, err)
			require.NotNil(t, got)
			mhc.AssertExpectations(t)
		})
	}
}

func TestClient_CreateRelease_NonAPIErrors(t *testing.T) {
	tests := []struct {
		name       string
		res        *http.Response
		err        error
		wantErrMsg string
	}{
		{
			name:       "failed_to_call_api",
			err:        fmt.Errorf("something went wrong"),
			wantErrMsg: "failed to do request:",
		},
		{
			name: "not_json_error_response",
			res: &http.Response{
				StatusCode: http.StatusBadRequest,
				Body:       ioutil.NopCloser(strings.NewReader("<not json>")),
			},
			wantErrMsg: "failed to decode error response:",
		},
		{
			name: "not_json_success_response",
			res: &http.Response{
				StatusCode: http.StatusCreated,
				Body:       ioutil.NopCloser(strings.NewReader("<not json>")),
			},
			wantErrMsg: "failed to decode response:",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mhc := &MockHTTPClient{}
			mhc.On("Do", mock.Anything).Return(tt.res, tt.err).Once()
			client, err := New("", "token", "projectID", mhc)
			require.NoError(t, err)

			res, err := client.CreateRelease(context.Background(), &CreateReleaseRequest{})
			require.Nil(t, res)
			require.Error(t, err)

			require.Contains(t, err.Error(), tt.wantErrMsg)

			if tt.res != nil {
				mhc.AssertExpectations(t)
			}
		})
	}
}
