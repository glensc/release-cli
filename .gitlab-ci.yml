.go-cache:
  variables:
    GOPATH: $CI_PROJECT_DIR/.go
  before_script:
    - mkdir -p .go
  cache:
    paths:
      - .go/pkg/mod/

include:
  - template: Security/License-Management.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml

variables:
  GO: "1.13.8"

image: golang:${GO}

stages:
  - test
  - security
  - build
  - publish

license_management:
  stage: security

sast:
  stage: security

build:
  extends: .go-cache
  stage: build
  script:
    - make mocks-check
    - make deps-check
    - make build
  artifacts:
    paths:
      - ./bin/release-cli
    expire_in: 7 days

docker-build:
  extends: .go-cache
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p .docker-cache
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --no-push --cache-dir .docker-cache
  artifacts:
    paths:
      - .docker-cache
    expire_in: 1 day

docker-push:
  stage: publish
  rules:
  - if: '$CI_COMMIT_REF_NAME == "master" || $CI_COMMIT_TAG'
    when: always
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # uses `latest` tag when $CI_COMMIT_TAG == ""
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG --cache-dir .docker-cache
  needs:
    - job: docker-build
      artifacts: true

test:
  extends: .go-cache
  stage: test
  script:
    - make test

test-race:
  extends: .go-cache
  stage: test
  image: golang:${GO}
  variables:
    CGO_ENABLED: 1
  script:
    - make test-race

lint:
  extends: .go-cache
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine
  stage: test
  script:
    # Use default .golangci.yml file from the image if one is not present in the project root.
    - '[ -e .golangci.yml ] || cp /golangci/.golangci.yml .'
    # Write the code coverage report to gl-code-quality-report.json
    # and print linting issues to stdout in the format: path/to/file:line description
    - golangci-lint run --out-format code-climate | tee gl-code-quality-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json
    expire_in: 7 days
  allow_failure: true

cover:
  extends: .go-cache
  stage: test
  script:
    - make cover
  coverage: '/total:.+\(statements\).+\d+\.\d+/'
  artifacts:
    paths:
      - cover/coverage.html
    expire_in: 7 days
