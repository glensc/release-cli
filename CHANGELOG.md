# Changelog

## v0.1.0

- Create Release CLI module [!4](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/4)
- Add GitLab client package [!5](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/5)
- Add command `create` [!6](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/6)
- Add docker image [!13](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/13)
