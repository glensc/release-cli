REVISION := $(shell git rev-parse --short HEAD || echo unknown)
LAST_TAG := $(shell git describe --tags --abbrev=0)
COMMITS := $(shell echo `git log --oneline $(LAST_TAG)..HEAD | wc -l`)
VERSION := $(shell cat VERSION)
CI_REGISTRY ?= registry.gitlab.com/gitlab-org/release-cli

# default to CGO_ENABLED=0 but can be overriden
CGO_ENABLED := $(if $(CGO_ENABLED),$(CGO_ENABLED),0)

ifneq (v$(VERSION),$(LAST_TAG))
	VERSION := $(shell echo $(VERSION)~beta.$(COMMITS).g$(REVISION))
endif

VERSION_FLAGS := -ldflags='-X "main.VERSION=$(VERSION)"'

_allpackages = $(shell (go list ./...))

# memoize allpackages, so that it's executed only once and only if used
allpackages = $(if $(__allpackages),,$(eval __allpackages := $$(_allpackages)))$(__allpackages)

.PHONY: setup build run clean
setup: clean
	$Q mkdir -p bin/
	$Q mkdir -p cover/

build: setup
	echo $(allpackages)
	$Q go build $(VERSION_FLAGS) -o bin/$(PROJECT_NAME) ./cmd/$(PROJECT_NAME)

docker:
	$Q docker build -t ${CI_REGISTRY}:${REVISION} .

run: setup
	$Q go run cmd/$(PROJECT_NAME)/main.go

clean:
	$Q rm -rf bin/*
	$Q rm -rf cover/*
