GOLANGCI_LINT_IMAGE := registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine
MOCKERY = mockery
DEVELOPMENT_TOOLS = $(MOCKERY)

.PHONY: lint test cover list deps-check mocks

lint:
	docker run -v $(PWD):/app -w /app $(GOLANGCI_LINT_IMAGE) sh -c "golangci-lint run --out-format code-climate  $(if $V,-v) | tee gl-code-quality-report.json | jq -r '.[] | \"\(.location.path):\(.location.lines.begin) \(.description)\"'"

test:
	go test $(if $V,-v) $(allpackages) -count 1 -timeout 30s

test-race:
	go test $(if $V,-v) $(allpackages) -count 1 -timeout 30s -race


# The acceptance tests cannot count for coverage
cover: setup
	@echo "NOTE: make cover does not exit 1 on failure, don't use it to check for tests success!"
	$Q rm -f cover/*.out cover/all.merged
	$Q go test ./... -coverprofile=cover/unit.out
	$Q go tool cover -html cover/unit.out -o cover/coverage.html
	@echo ""
	@echo "=====> Total test coverage: <====="
	@echo ""
	$Q go tool cover -func cover/unit.out

list:
	@echo $(allpackages)

deps-check:
	go mod tidy
	@if git diff --color=always --exit-code -- go.mod go.sum; then \
		echo "go.mod and go.sum are ok"; \
	else \
    echo ""; \
		echo "go.mod and go.sum are modified, please commit them";\
		exit 1; \
  fi;

mocks-check: mocks
	@if git diff --color=always --exit-code -- *mock_*; then \
    		echo "mocks are ok"; \
    	else \
        echo ""; \
    		echo "mocks are modified, please commit them";\
    		exit 1; \
    fi;

# development tools
mocks: $(MOCKERY)
	find . -type f ! -path '*vendor/*' -name 'mock_*' -delete && \
	${GOPATH}/bin/mockery -dir=./internal/gitlab/ -all -inpkg -testonly -case snake

$(MOCKERY):
	go get github.com/vektra/mockery/cmd/mockery

